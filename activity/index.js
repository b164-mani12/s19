console.log("s19 Activity");

//3,4
const num = 10;
const getCube = Math.pow(num, 3);
console.log(`The cube of ${num} is ${getCube}`);

//5
const address = ["blk2", "lot2", "brgy.campetic", "palo", "leyte"];

//6
const [blk, lot, brgy, city, province] = address
console.log(`I live in ${blk} ${lot} ${city} ${province}`);

//7
const animal = {
	name: "lion",
	type: "cat",
	legs: "4"
}

//8
const {name,type,legs} = animal;
console.log(`my animal pet is a ${name} under the family of ${type} and has ${legs} legs`);

//9
const numbers = [1,2,3,4,5];

//10
numbers.forEach((numbers) => {
	console.log(`${numbers}`);
})

//11
const i = 0;
const reduceNumber = numbers.reduce((pValue, cValue) => pValue + cValue, i );
console.log(reduceNumber);

//12
class Dog {
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

//13
const myDog = new Dog("tofu",7,"askal");
console.log(myDog);
	